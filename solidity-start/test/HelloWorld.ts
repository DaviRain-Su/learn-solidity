import { time, loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { anyValue } from "@nomicfoundation/hardhat-chai-matchers/withArgs";
import { expect } from "chai";
import { ethers } from "hardhat";


describe("HelloWorld", function () {
    // We define a fixture to reuse the same setup in every test.
    // We use loadFixture to run this setup once, snapshot that state,
    // and reset Hardhat Network to that snapshot in every test.
    async function deployOneYearLockFixture() {  
      // Contracts are deployed using the first signer/account by default
      const [owner, otherAccount] = await ethers.getSigners();
  
      const HeloWorld = await ethers.getContractFactory("HelloWorld")
      const hello_world = await HeloWorld.deploy();
  
      return { hello_world, owner, otherAccount };
    }

    describe("Deployment", function () {
        it("Read Hello World Content", async function () {
          const { hello_world } = await loadFixture(deployOneYearLockFixture);
            
          expect(await hello_world.hello_world()).to.equal("Hello World!");
          // console.log(await hello_world.hello_world());
        });
    });

});