import { time, loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { anyValue } from "@nomicfoundation/hardhat-chai-matchers/withArgs";
import { expect } from "chai";
import { ethers } from "hardhat";


describe("ValueTypes", function () {
    // We define a fixture to reuse the same setup in every test.
    // We use loadFixture to run this setup once, snapshot that state,
    // and reset Hardhat Network to that snapshot in every test.
    async function deployOneYearLockFixture() {  
      // Contracts are deployed using the first signer/account by default
      const [owner, otherAccount] = await ethers.getSigners();
  
      const ValueTypes = await ethers.getContractFactory("ValueTypes")
      const value_types = await ValueTypes.deploy();
  
      return { value_types, owner, otherAccount };
    }

    describe("Deployment", function () {
        it("ValueTypes bool Content Test", async function () {
          const { value_types } = await loadFixture(deployOneYearLockFixture);
            
          expect(await value_types._bool()).to.equal(true);
          expect(await value_types._bool1()).to.equal(false);
          expect(await value_types._bool2()).to.equal(false);
          expect(await value_types._bool3()).to.equal(true);
          expect(await value_types._bool4()).to.equal(false);
          expect(await value_types._bool5()).to.equal(true);
        //   console.log(await value_types._bool());
        });

        it("ValueTypes int Content Test", async function () {
            const { value_types } = await loadFixture(deployOneYearLockFixture);
              
            expect(await value_types._int()).to.equal(-1);
            expect(await value_types._uint()).to.equal(1);
            expect(await value_types._uint256()).to.equal(2000);
            expect(await value_types._number1()).to.equal(2001);
            expect(await value_types._number2()).to.equal(1999);
            expect(await value_types._number3()).to.equal(4000);
            expect(await value_types._number4()).to.equal(1000);
            expect(await value_types._number5()).to.equal(4);
            expect(await value_types._number6()).to.equal(1);
            expect(await value_types._result1()).to.equal(true);
            expect(await value_types._result2()).to.equal(true);
            expect(await value_types._result3()).to.equal(true);
            expect(await value_types._result4()).to.equal(true);
            // console.log(await value_types._int());
            // console.log(await value_types._uint());
        });

        it("ValueTypes address Content Test", async function () {
            const { value_types } = await loadFixture(deployOneYearLockFixture);
                
            expect(await value_types._address()).to.equal("0x7A58c0Be72BE218B41C608b7Fe7C5bB630736C71");
            expect(await value_types._address1()).to.equal("0x7A58c0Be72BE218B41C608b7Fe7C5bB630736C71");
            expect(await value_types.balance()).to.equal(0);
            // console.log(await value_types._address());
        });


        it("ValueTypes address Content Test", async function () {
            const { value_types } = await loadFixture(deployOneYearLockFixture);
                
            expect(await value_types._bytes32()).to.equal("0x4d696e536f6c6964697479000000000000000000000000000000000000000000");
            expect(await value_types._byte()).to.equal("0x4d");
        });

        it("ValueTypes enum Content Test", async function () {
            const { value_types } = await loadFixture(deployOneYearLockFixture);
                
            expect(await value_types.enumToUint()).to.equal(0);
            // console.log(await value_types.enumToUint());
        });
    });

});