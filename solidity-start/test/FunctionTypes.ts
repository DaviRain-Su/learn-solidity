import { time, loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { anyValue } from "@nomicfoundation/hardhat-chai-matchers/withArgs";
import { expect } from "chai";
import { ethers } from "hardhat";


describe("FunctionTypes", function () {
    // We define a fixture to reuse the same setup in every test.
    // We use loadFixture to run this setup once, snapshot that state,
    // and reset Hardhat Network to that snapshot in every test.
    async function deployOneYearLockFixture() {  
      // Contracts are deployed using the first signer/account by default
      const [owner, otherAccount] = await ethers.getSigners();
  
      const FunctionType = await ethers.getContractFactory("FunctionTypes")
      const function_type = await FunctionType.deploy();
  
      return { function_type, owner, otherAccount };
    }

    describe("Deployment", function () {
        it("Read FunctionType Content", async function () {
          const { function_type } = await loadFixture(deployOneYearLockFixture);
            
          expect(await function_type.number()).to.equal(5);
        });

        it("Operatr add FunctionType Content", async function () {
          const { function_type } = await loadFixture(deployOneYearLockFixture);
            
          expect(await function_type.number()).to.equal(5);
          await function_type.add();
          expect(await function_type.number()).to.equal(6);
          expect(await function_type.addView()).to.equal(7);
        });

        it("Operatr add View FunctionType Content", async function () {
          const { function_type } = await loadFixture(deployOneYearLockFixture);
            
          expect(await function_type.number()).to.equal(5);
          expect(await function_type.addView()).to.equal(6);
        });

        it("Operatr add View FunctionType Content", async function () {
          const { function_type } = await loadFixture(deployOneYearLockFixture);
            
          expect(await function_type.number()).to.equal(5);
          expect(await function_type.addPure(3)).to.equal(4);
        });

        it("Operatr minusCall  FunctionType Content", async function () {
          const { function_type } = await loadFixture(deployOneYearLockFixture);
            
          expect(await function_type.number()).to.equal(5);
          await function_type.minusCall();
          expect(await function_type.number()).to.equal(4);
        });

        it("Operatr minusPayable  FunctionType Content", async function () {
          const { function_type } = await loadFixture(deployOneYearLockFixture);
            
          expect(await function_type.number()).to.equal(5);
          // TODO(need fix)
          // expect(await function_type.minusPayable()).to.equal(100);
          expect(await function_type.number()).to.equal(5);
        });
    });

});