# ValueTypes

- solidity变量类型有
  - 数值类型
  - 引用类型
  - 映射类型
  - 函数类型
- solidity中数值类型包括；uint8，address， bool
- address payable addr; addr.transfer(1); 意思是合约向addr转账1wei
- bytes4的类型具有8个16进制位
- 以下运算能使a返回true的是？
  - bool a = 1+1!=2||0/1==1; //false
  - bool a = 1+1!=2||1-1>0; // false
  - bool a = 1+1==2&&1**2==2; // false
  - bool a = 1-1==0&&1%2==1; // true