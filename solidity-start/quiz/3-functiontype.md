# Function type

- solidity 中函数可见性说明符
  - external
  - private
  - public
  - internal
- view关键字描述
  - 包含 view 关键字的函数，可以读取但不能写入存储在链上的状态变量
- pure和view关键字描述
  - 这两个关键字可以控制函数的权限
  - 包含这两个关键字的函数，调用时都不消耗 gas
  - 其他编程语言中，一般不包含这两个关键字