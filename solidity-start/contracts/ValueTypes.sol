// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.4;

// Solidity中的变量类型
// 1.数值类型， 包括布尔类型，整数类型，这类变量赋值时直接传递数值
// 2. 引用类型， 包括数组合结构体，这类变量占空间大，赋值时候直接传递地址（类似指针）
// 3. 映射类型，solidity中的哈希表
// 4. 函数类型，solidity文档里把函数归到数值类型。

contract ValueTypes {

    // 1. -------- 布尔类型 ----------------
    bool public _bool = true;

    // ！逻辑非
    bool public _bool1 = !_bool; // false
    // && 逻辑与
    bool public _bool2 = _bool && _bool1; // false
    // || 逻辑或 
    bool public _bool3 = _bool || _bool1; // true
    // == 等于
    bool public _bool4 = _bool == _bool1; // false
    // != 不等于
    bool public _bool5 = _bool != _bool1; // true
    
    // && 和 || 具有短路求值的特点

    // 2. -------- 整型 ----------------

    // 整数
    int public _int = -1; 
    // 正整数
    uint public _uint = 1;
    // 256位正整数
    uint256 public _uint256 = 2000;

    // + 
    uint256 public _number1 = _uint256 + 1; // 2001
    // - 
    uint256 public _number2 = _uint256 - 1; // 1999
    // * 
    uint256 public _number3 = _uint256 * 2; // 4000
    // / 
    uint256 public _number4 = _uint256 / 2; // 1000
    // ** 
    uint256 public _number5 = 2 ** 2; // 4
    // % 
    uint256 public _number6 = 7 % 2; // 1
    // <= , <
    bool public _result1 = 2 <= 3; // true
    // >=, >
    bool public _result2 = 2 >= 1; // true
    // != 
    bool public _result3 = 2 != 3; // true
    // == 
    bool public _result4 = 1 == 1; // true


     // 3. -------- 地址类型 ----------------
     
     // address
     address public _address = 0x7A58c0Be72BE218B41C608b7Fe7C5bB630736C71;
     address payable public _address1 = payable(_address); // payable address，可以转账、查余额
     // 地址类型的成员
    uint256 public balance = _address1.balance; // balance of address


    // 4. -------- 定长字节数组 ----------------

    bytes32 public _bytes32 = "MinSolidity";
    // MiniSolidity变量以字节的方式存储进变量_byte32，
    // 转换成16进制为：0x4d696e69536f6c69646974790000000000000000000000000000000000000000
    bytes1 public _byte = _bytes32[0];
    // _byte变量存储_byte32的第一个字节，为0x4d。


    // 4. -------- 枚举 ----------------
     
    // enum 是solidity中用户定义的数据类型，它主要用于uint分配名称，是程序易于阅读和维护。他与
    // C语言中的enum类似，把名称从0开始uint表示。
    
    // 用enum将uint 0， 1， 2表示为Buy, Hold, Sell
    enum ActionSet {
        Buy,  // 0
        Hold, // 1
        Sell // 2
    }
    // create enum action
    ActionSet action = ActionSet.Buy;

    // 它可以显式的和uint相互转换，并会检查转换的正整数是否在枚举的长度内，不然会报错：
    function enumToUint() external view returns(uint) {
        // ActionSet action = ActionSet.Buy;
        return uint(action);
    }

}