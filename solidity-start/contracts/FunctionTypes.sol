// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

//function (<parameter types>) {internal|external|public|private} [pure|view|payable] [returns (<return types>)]
// 1. function 声明函数时固定的写法，
// 2. (<parameter type>): 圆括号里写函数的参数，也就是要输入到函数的变量类型和名字
// 3. {internal | external | public | private } 函数可见性说明符，一共四种，没有标明函数的类型，默认internal
// - public： 内部外部均可见（也可以用于修饰状态变量，public变量会自动生成getter函数，用于查询数值）
// - private: 只能从本合约内部访问。继承的合约也不能用，(也可以用于修饰状态变量)
// - external: 只能从合约外部访问 但是可以用this.f()来调用，f是函数名
// - internal: 只能从合约内部访问，继承的合约可以用（也可以用于修饰状态变量）
// 4. [pure | view | payable] 决定函数权限 功能的关键字，payable可支付，带他的函数，运行时可以给合约转入ETH，
// - pure： 包含pure关键字的函数，不能读取也不能写入存储在链上的状态变量
// - view: 能读取但也不能写入状态变量，
// - 不写pure 也不写view，函数既可以读取也可以写入状态变量
// 5. [returns()] 函数返回的变量类型和名称

contract FunctionTypes{

    uint256 public number = 5;

    // Function cannot be declared as pure 
    // because this expression (potentially) modifies the state.
    // function add() pure external {
    //     number += 1;
    // }

    // Function cannot be declared as view 
    // because this expression (potentially) modifies the state.
    // function add() view external {
    //     number += 1;
    // }

    function add() external {
        number += 1;
    }


    // pure usage 
    function addPure(uint256 _number) external pure returns(uint256 new_number) {
        new_number = _number + 1;
    }

    // view usage 
    function addView() external view returns(uint256 new_number) {
        new_number = number + 1;
    }

    // internal usage 
    function minus() internal {
        number -= 1;
    }

    // 合约内的函数可以调用内部函数
    // 我们定义一个internal的minus()函数，每次调用使得number变量减1。由于是internal，只能由合约内部调用，而外部不能。因此，
    // 我们必须再定义一个external的minusCall()函数，来间接调用内部的minus()
    function minusCall() external {
        minus();
    }
    
    // payable: 递钱，能给合约支付eth的函数
    // 我们定义一个external payable的minusPayable()函数，间接的调用minus()，
    // 并且返回合约里的ETH余额（this关键字可以让我们引用合约地址)。 
    // 我们可以在调用minusPayable()时，往合约里转入1个ETH
    function minusPayable() external payable returns(uint256 balance) {
        minus();
        balance = address(this).balance;
    }
}