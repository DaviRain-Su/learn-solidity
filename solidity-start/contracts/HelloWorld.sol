// SPDX-License-Identifier: UNLICENSED
// 第一行是注释，会写一下这个代码所用的软件许可。如果不写许可，编译会警告，但是程序可以运行。
// solidity的注释由 `//`开头，后面跟着注释的内容。

// Solidity 是以太坊虚拟机智能合约语言。

// 这里声明了文件所用的solidity的版本，因为不同版本的语法有差别。
// 这行代码的意思是源文件将不允许低于0.8.4版本并且不高于0.9.0的编译器编译(第二个条件由^提供)。
// solidity语句以分号结尾。
// pragma solidity ^0.8.4;

// // 创建合约，并声明合约的名字是HelloWorld
// contract HelloWorld {
//     // 声明了一个字符串变量hello_world,并给他赋值 "Hello World!"
//     string public hello_world = "Hello World!";
// }   
// compiler version must be greater than or equal to 0.8.13 and less than 0.9.0
pragma solidity ^0.8.13;

contract HelloWorld {
    string public greet = "Hello World!";
}